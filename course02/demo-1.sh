#!/bin/bash
cd course02
# build order-service image
docker-compose build 
# Start our kafka cluster
docker-compose up -d kafka-cluster
# Wait 2 minutes for the kafka cluster to be started

# FileStreamSourceConnector in standalone mode

# We start a hosted tools, mapped on our code
# Linux / Mac
docker run --rm -it -v "$(pwd)":/tutorial --net=host lensesio/fast-data-dev:2.3 bash
# Windows Command Line:
docker run --rm -it -v %cd%:/tutorial --net=host lensesio/fast-data-dev:2.3 bash
# Windows Powershell:
docker run --rm -it -v ${PWD}:/tutorial --net=host lensesio/fast-data-dev:2.3 bash


cd tutorial/demo-1
# create the topic we write to with 3 partitions
kafka-topics --create --topic demo-1-standalone --partitions 3 --replication-factor 1 --zookeeper 127.0.0.1:2181

# we launch the kafka connector in standalone mode:
# Usage is connect-standalone worker.properties connector1.properties [connector2.properties connector3.properties]
connect-standalone worker.properties file-stream-demo-standalone.properties

# start console-consumer from another terminal
kafka-console-consumer --bootstrap-server localhost:9092 --topic demo-1-standalone --from-beginning

# write some data to the demo-file.txt !
# shut down the terminal when you're done.
