# Apache Kafka training courses

## Course plan
- [Kafka Core Concepts](https://gitlab.com/ddt-enterprise-architecture/kafka-training/-/tree/master/course01)

![course01](images/course01.png)
---

- [Kafka Connect & CDC](https://gitlab.com/ddt-enterprise-architecture/kafka-training/-/tree/master/course02)
![course02](images/course02.png)
---

- Kafka Stream
![course03](images/course03.png)