# Apache Kafka training courses

[<img src="slide/Course01-Kafka-Core-Concepts.png">](slide/Course01-Kafka-Core-Concepts.pdf)
---

## Kafka Installation
- Start Zookeeper & Kafka
```bash
docker-compose up -d
```
- Verify Zookeeper & Kafka services
```bash
docker-compose ps
```
- Verify Zookeeper is healthy
```bash
docker-compose logs zookeeper | grep -i binding

zookeeper_1  | [2019-12-31 01:56:36,468] INFO binding to port 0.0.0.0/0.0.0.0:2181 (org.apache.zookeeper.server.NIOServerCnxnFactory)
```
- Verify Kafka is healthy
```bash
docker-compose logs kafka | grep -i started

kafka_1      | [2019-12-31 01:56:40,051] INFO [SocketServer brokerId=1] Started 2 acceptor threads (kafka.network.SocketServer)
kafka_1      | [2019-12-31 01:56:40,400] INFO [ReplicaStateMachine controllerId=1] Started replica state machine with initial state -> Map() (kafka.controller.ReplicaStateMachine)
kafka_1      | [2019-12-31 01:56:40,406] INFO [PartitionStateMachine controllerId=1] Started partition state machine with initial state -> Map() (kafka.controller.PartitionStateMachine)
kafka_1      | [2019-12-31 01:56:40,439] INFO [SocketServer brokerId=1] Started processors for 2 acceptors (kafka.network.SocketServer)
kafka_1      | [2019-12-31 01:56:40,442] INFO [KafkaServer id=1] started (kafka.server.KafkaServer)
```

## Basic command
- Get into Kafka container

```bash
docker-compose exec kafka bash
```
- Create a topic
```bash
kafka-topics --create --zookeeper zookeeper:2181 \
  --replication-factor 1 --partitions 1 --topic test
```

- Create a topic with 2 partition
```
kafka-topics --create --zookeeper zookeeper:2181 \
  --replication-factor 1 --partitions 2 --topic test-2
```

- list out all kafka topic
```bash
kafka-topics --list --zookeeper zookeeper:2181
```
- describe specific kafka topic
```bash
kafka-topics --describe --zookeeper zookeeper:2181 --topic test
```
- publish data to specific kafka topic
```bash
kafka-console-producer --broker-list localhost:9092 --topic test
>Hello Kafka
>Hello World
```
- subscribe specific kafka topic
```bash
kafka-console-consumer --bootstrap-server localhost:9092 \
  --topic test --from-beginning
```

## Getting kafka consumer offsets of a consumer group

- list consumer groups
```bash
kafka-consumer-groups.sh  --list --bootstrap-server localhost:9092
```
- describe consumer group and list offset info
```bash
kafka-consumer-groups.sh --bootstrap-server localhost:9092 --describe --group group1 
```

- To see the members of the first group
```bash
./bin/kafka-consumer-groups.sh --describe --group order-consumer-group-1 --members --bootstrap-server localhost:9092

GROUP                  CONSUMER-ID                                                            HOST            CLIENT-ID                         #PARTITIONS
order-consumer-group-1 consumer-order-consumer-group-1-1-f1ffe21c-8c9d-493f-92bf-402c100517d3 /192.168.0.1    consumer-order-consumer-group-1-1 1
```

## Reset offset of a specific group
- There are many other resetting options, run kafka-consumer-groups for details
```bash
--shift-by <positive_or_negative_integer>
--to-current
--to-latest
--to-offset <offset_integer>
--to-datetime <datetime_string>
--by-duration <duration_string>
```
- reset the consumer offset for a topic (preview)
```bash
kafka-consumer-groups.sh --bootstrap-server localhost:9092 --group group1 --topic messages --reset-offsets --to-offset 2 --dry-run
```
- execute the reset and reset the consumer group offset
```bash
kafka-consumer-groups.sh --bootstrap-server localhost:9092 --group group1 --topic messages --reset-offsets --to-offset 2 --execute
```