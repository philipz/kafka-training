package com.ddt.das.ea.producer;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.*;

import java.util.Properties;

/**
 * @author rich
 * Sending a Message Synchronously
 */
@Slf4j
public class SyncProducer {
    public static void main(String[] args) {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.ACKS_CONFIG, "all");   // Set number of acks (default is all)

        String topicName = "test";
        int msgCount = 1000;

        try (Producer<String, String> producer = new KafkaProducer<>(props)) {
            try {
                log.info("Start sending messages ...");

                for (int i = 0; i < msgCount; i++) {
                    RecordMetadata recordMetadata = producer.
                            send(new ProducerRecord<>(topicName, "" + i, "msg_" + i))
                            .get();

                    log.info("Metadata - Topic: {}, Partition: {}, Offset: {} ",
                            recordMetadata.topic(),
                            recordMetadata.partition(),
                            recordMetadata.offset());
                }
            } catch (Exception e) {
                log.error("Unexpected error!", e);
            }
        }
        log.info("Message sending completed!");
    }
}
